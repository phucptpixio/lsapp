# CI/CD pipeline for company

## Deployment process

NewCMS is a web application build with laravel.
Pixio studio Deployment process

__Table of contents__


* [ Installation ](#markdown-header-requires)
* [ Development ](#markdown-header-development)

## Installation
Prerequisites: __php (>=7.2, 7.3 preffered), composer version >=1.6.x__, __Mysql 5.7.x__ and __Git__.

* [Install php7.3](https://www.php.net/downloads)
* [Install php 7.3 on ubuntu 18.04](https://www.cloudbooklet.com/how-to-install-php-7-3-on-ubuntu-18-04/)
* Deployer
* 


If you don't have Composer, install it by running (this will install composer __globally__):

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
```

Install dependencies via composer:

```
$ composer install 
```

Import test database:

```
$ mysql -u your_username -p your_password <databasename> < spyets_newcms.sql
```

Config environment at `.env` file, example at `.env.example`

```
$ cp .env.example .env
```

Generate laravel Application key 

```
$ php artisan key:generate
```

## Development

Running development mode with artisan:

```
$ php artisan serve
```






