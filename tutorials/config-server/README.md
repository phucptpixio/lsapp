# How to config a server (Ubuntu 18.04) for a laravel project

## Table of contents

* [Getting Started](#markdown-header-getting-started)
* [Lock Down to SSH Key only](#markdown-header-lock-down-to-ssh-key-only)
* [Disable Password from Server](#markdown-header-disable-password-from-server)
* [Setting up Firewall](#markdown-header-setting-up-firewall)
* [Install Linux, Nginx, MySQL, PHP](#markdown-header-install-linux,-nginx,-mysql,-php)
* [PHP & Basic Nginx](#markdown-header-php-&-basic-nginx)
* [Laravel Ecosystem](#markdown-header-laravel-ecosystem)
* [Modify Nginx](#markdown-header-modify-nginx)
* [Let's Encrypt](#markdown-header-use-let-encrypt-to-enable-ssl)


## Getting started

+ Have a server (ubuntu, centos, ...). Here we use droplet with ubuntu 18.04
+ `ssh root@[SERVER IP ADDRESS]`
+ Get password from your email
+ Change password on first login
+ `adduser laravel`
+ Enter password and other information
+ `usermod -aG sudo laravel`

## Lock Down to SSH Key only (Extremely Important)

+ In your local machine, `ssh-keygen`
+ Generate a key, if you leave passphrase blank, no need for password
+ `ls ~/.ssh` to show files in local machine
+ Get the public key, `cat ~/.ssh/id_rsa.pub`
+ Copy it 
+ `cd ~/.ssh` and `vim authorized_keys`
+ Paste key
+ Repeat steps for laravel user
+ `su laravel` then `mkdir ~/.ssh` fix permissions `chmod 700 ~/.ssh`
+ `vim ~/.ssh/authorized_keys` and paste key
+ `chmod 600 ~/.ssh/authorized_keys` to restrict this from being modified
+ `exit` to return to root user

## Disable Password from Server

+ `sudo vim /etc/ssh/sshd_config`
+ Find PasswordAuthentication and set that to `no`
+ Turn on `PubkeyAuthentication yes`
+ Turn off `ChallengeResponseAuthentication no`
+ Reload the SSH service `sudo systemctl reload sshd`
+ Test new user in a new tab to prevent getting locked out


## Setting up Firewall

+ View all available firewall settings
+ `sudo ufw app list`
+ Allow on OpenSSH so we don't get locked out
+ `sudo ufw allow OpenSSH`
+ Enable Firewall
+ `sudo ufw enable`
+ Check the status
+ `sudo ufw status`

## Install Linux, Nginx, Mysql, PHP

## PHP & Basic Nginx

## Laravel Ecosystem

## Modify Nginx

## Use Let Encrypt to enable SSL
